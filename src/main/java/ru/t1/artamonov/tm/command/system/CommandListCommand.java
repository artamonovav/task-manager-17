package ru.t1.artamonov.tm.command.system;

import ru.t1.artamonov.tm.api.model.ICommand;
import ru.t1.artamonov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show command list.";

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Display program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
