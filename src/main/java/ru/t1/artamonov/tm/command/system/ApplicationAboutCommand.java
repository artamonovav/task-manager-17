package ru.t1.artamonov.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display developer info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name:    Anatoly Artamonov");
        System.out.println("E-mail:  aartamonov@t1-consulting.ru");
    }

}
